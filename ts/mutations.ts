function createUser(data: any) {
    return `
        mutation {
            createUser(
                data: {
                    firstName: "${data.firstName}"
                    lastName: "${data.lastName}"
                    user: "${data.user}"
                    role: { connect: {id: "${data.role}"} }
                    email: "${data.email}"
                    birthDate: "${data.birthDate}"
                    address: "${data.address}"
                    city: "${data.city}"
                    password: "${data.password}"
                    phone: "${data.phone}"
                }
            ) { id }
        }
    `
}

function updateUser(data: any) {
    return `
        mutation{
            updateUser( where: {id: "${data.id}"}
                data: {
                    firstName: "${data.firstName}"
                    lastName: "${data.lastName}"
                    phone: "${data.phone}"
                    address: "${data.address}"
                    city: "${data.city}"
                    password: "${data.password}"
                }
            ) { id }
        }
    `
}

function mutationService(data: any, isUpdate: boolean = false) {
    return `
        mutation {
            ${isUpdate ? `updateService(where:{id:"${data.id}"} ` : 'createService( '}
                data: {
                    title: "${data.title}"
                    description: "${data.description}"
                    images: { create: [
                        ${data.images}
                    ]}
                }
            ) { id }
        }
    `
}

export {
    createUser,
    updateUser,
    mutationService
}