const servicesData = `
    services {
        id 
        title
        description
        images {
            src
            alt
        }
    }

`;

const images = `query {
    images {
        src
        alt
    }
}`;

const queryAuthUser = `query ($user: String!, $password: String!) {
    users (where: { user: $user, password: $password }) {
        firstName
        lastName
        user
        role {
            name
            menu_option {
                name
                path
            }
        }
        email
        birthDate
        address
        city
        password
        phone
    }
}`;

const queryAllUsers = `
    users {
        id
        user
        firstName
        lastName
        birthDate
        address
        email
        city
        phone
        role {
            name
        }
        descripcion
        services {
            id 
            title
            description
            images {
                src
                alt
            }
        }
        photo
    }`;

const queryRoles = `
    user_rols {
        id
        name
    }`;

const benefitData = `
    benefits {
        name
        image
        icon
        shortDescription
        longDescription
    }
`;
function getQuery( queries: String[] ) {
    return `query {
        ${ queries.join('\n') }
    }`;
}

export {
    servicesData,
    images,
    queryAuthUser,
    queryAllUsers,
    queryRoles,
    getQuery,
    benefitData
};