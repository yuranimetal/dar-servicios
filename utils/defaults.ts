const defaultImage = {
    src: 'https://stockpictures.io/wp-content/uploads/2020/01/image-not-found-big-768x432.png',
    alt: 'Default Image'
}

function getImage ( item: any ) {
    if ( item && item.images && item.images[0] ) {
        return item.images[0];
    }
    return defaultImage;
}

function getObjs (val: string, listObj: any[], strict: Boolean = true, several: Boolean = true, fiels: string[] = []) {
    if (!listObj) {
        return several ? [] : null;
    }
    if (!val || val.toString().trim() === '') {
        return several ? listObj : (listObj ? (listObj.length>0 ? listObj[0] : null): []);
    }

    val = val.trim().toLowerCase();

    const searchCallback = (obj: any): boolean => {
        return ((fiels && fiels.length>0) ? fiels : Object.keys(obj)).find(k => {
            if (!obj[k]) {
                return false;
            } else if (typeof obj[k]==='string' || typeof obj[k]==='number') {
                const currentVal = obj[k].toString().trim().toLowerCase();
                return strict ? currentVal===val :
                    val.split(' ').find((word) => currentVal.indexOf(word)>=0)!==undefined;
            }
            return callback(obj[k]);
        }) !== undefined;
    }
    const callback = ( obj: any ): boolean => {
        return (obj instanceof Array) ? obj.find(iter => searchCallback(iter))!==undefined :
            searchCallback(obj);
    }

    return several ? listObj.filter( callback ) : listObj.find( callback );
}

export {
    defaultImage,
    getImage,
    getObjs
}